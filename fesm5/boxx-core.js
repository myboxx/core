import { __decorate } from 'tslib';
import { NgModule, InjectionToken } from '@angular/core';

var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        NgModule({
            declarations: [],
            imports: [],
            exports: []
        })
    ], CoreModule);
    return CoreModule;
}());

var AbstractAppConfigService = /** @class */ (function () {
    function AbstractAppConfigService() {
    }
    AbstractAppConfigService.prototype.instance = function () {
        return this.instanceName;
    };
    AbstractAppConfigService.prototype.baseUrl = function () {
        return this.apiUrl;
    };
    return AbstractAppConfigService;
}());
var APP_CONFIG_SERVICE = new InjectionToken('appConfigService');

/*
 * Public API Surface of core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { APP_CONFIG_SERVICE, AbstractAppConfigService, CoreModule };
//# sourceMappingURL=boxx-core.js.map
