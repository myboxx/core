export interface IStateSuccessBase {
    after: string;
    data?: any;
}
export interface IStateErrorBase {
    after: string;
    error: any;
}
