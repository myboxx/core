import { InjectionToken } from '@angular/core';
export declare abstract class AbstractAppConfigService {
    constructor();
    protected instanceName: string;
    protected apiUrl: string;
    instance(): string;
    baseUrl(): string;
}
export declare const APP_CONFIG_SERVICE: InjectionToken<AbstractAppConfigService>;
