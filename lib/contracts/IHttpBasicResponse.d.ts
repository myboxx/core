export interface IHttpBasicResponse<T> {
    data?: T;
    error?: {
        message: any;
    };
    message?: any;
    status: 'success' | 'error';
    statusText?: string;
    statusCode?: number;
}
