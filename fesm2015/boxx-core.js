import { __decorate } from 'tslib';
import { NgModule, InjectionToken } from '@angular/core';

let CoreModule = class CoreModule {
};
CoreModule = __decorate([
    NgModule({
        declarations: [],
        imports: [],
        exports: []
    })
], CoreModule);

class AbstractAppConfigService {
    constructor() { }
    instance() {
        return this.instanceName;
    }
    baseUrl() {
        return this.apiUrl;
    }
}
const APP_CONFIG_SERVICE = new InjectionToken('appConfigService');

/*
 * Public API Surface of core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { APP_CONFIG_SERVICE, AbstractAppConfigService, CoreModule };
//# sourceMappingURL=boxx-core.js.map
